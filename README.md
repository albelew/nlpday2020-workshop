# NLPday 2020

Monitoring environment based on satellite data with Python and PySpark

### Requirements

You need to have:

- installed Docker
- access to the internet

### Exercises

1. PySpark - NDVI
2. PySpark with RasterFrames - masking data and time-series analysis.
3. PySpark with RasterFrames - unsupervised learning.
